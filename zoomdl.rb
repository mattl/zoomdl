# This script downloads all the recordings from a Zoom account and
# uploads them to a directory on Google Drive. The first attempt
# to upload to Google Drive will attempt to authenticate and
# create a local config.json with the credentials.
require 'json'
require 'pp'
require 'net/http'
require 'date'
require 'open-uri'
require 'google_drive'

BASE_URL = 'https://api.zoom.us/v1/recording/list'
ZOOM_DELETE_URL = 'https://api.zoom.us/v1/recording/delete'

# Zoom API information
API_KEY = ''
API_SECRET = ''
HOST_ID = ''

GOOGLE_BASE_FOLDER = 'GitLab Videos'

def sanitize_filename(filename)
  filename.downcase.strip.gsub(' ', '-').gsub(/[^\w-]/, '')
end

def download_video(url, filename, file_size)
  STDERR.puts "Downloading #{filename}"
  download = open(url)
  bytes = IO.copy_stream(download, filename)

  if bytes != file_size
    puts "Mismatch in file size: downloaded #{bytes}, expected #{file_size}, retrying..."
    download = open(url)
    bytes = IO.copy_stream(download, filename)

    if bytes != file_size
      puts "Failed retry: downloaded #{bytes}, expected #{file_size}"
      exit
    end
  end
end

def zoom_page_total
  uri = URI(BASE_URL)
  res = Net::HTTP.post_form(uri, api_key: API_KEY, api_secret: API_SECRET, host_id: HOST_ID)
  zoom = JSON.parse(res.body)
  zoom['page_count']
end

def get_file_type(file_type)
  file_type = file_type.downcase

  return 'txt' if file_type == 'chat'

  file_type
end

def gdrive_file_valid?(topic, filename, file_size)
  session = GoogleDrive::Session.from_config('config.json')

  sub_directory = topic
  dest_folder = session.file_by_title([GOOGLE_BASE_FOLDER, sub_directory])

  return unless dest_folder

  dest_file = dest_folder.file_by_title(filename)

  puts "Checking for existence #{filename}..."

  if dest_file
    dest_file_size = dest_file.size.to_i

    if dest_file_size != file_size
      puts "Google Drive has #{filename} with #{dest_file_size} bytes, expecting #{file_size}"
    end

    dest_file_size == file_size
  else
    false
  end
end

def upload_video(topic, filename, file_size)
  session = GoogleDrive::Session.from_config('config.json')
  sub_directory = topic

  base_folder = session.file_by_title([GOOGLE_BASE_FOLDER])
  dest_folder = session.file_by_title([GOOGLE_BASE_FOLDER, sub_directory])

  unless dest_folder
    puts "Creating destination folder #{sub_directory}"
    dest_folder = base_folder.create_subcollection(sub_directory)
  end

  puts "Uploading to #{sub_directory}"
  dest_filename = dest_folder.file_by_title(filename)

  if dest_filename
    puts "File #{filename} already exists, removing"
    dest_filename.delete
  end

  uploaded = dest_folder.upload_from_file(filename)

  uploaded.size.to_i == file_size
end

def delete_recording_from_zoom(meeting_id, file_id)
  puts "Deleting meeting ID #{meeting_id}, file ID #{file_id} from Zoom"

  uri = URI(ZOOM_DELETE_URL)
  res = Net::HTTP.post_form(uri, api_key: API_KEY, api_secret: API_SECRET, meeting_id: meeting_id, file_id: file_id)

  zoom = JSON.parse(res.body)

  res.code.to_i == 200
end

def retrieve_videos(page_number)
  uri = URI(BASE_URL)
  res = Net::HTTP.post_form(uri, api_key: API_KEY, api_secret: API_SECRET, host_id: HOST_ID, page_number: page_number)
  zoom = JSON.parse(res.body)

  zoom['meetings'].each do |item|
    topic = item['topic']
    recording_files = item['recording_files']

    recording_files.each do |file|
      event_date = Date.rfc3339(file['recording_start']).to_s
      uuid = item['uuid']
      file_type = get_file_type(file['file_type'])
      filename = sanitize_filename("#{topic}-#{event_date}-#{uuid}") + ".#{file_type}"
      video_url = file['download_url']
      file_size = file['file_size']

      if gdrive_file_valid?(topic, filename, file_size)
        puts "Skipping #{filename}, already exists"
      else
        download_video(video_url, filename, file_size)
        success = upload_video(topic, filename, file_size)

        unless success
          puts "Failed to upload video #{filename}, aborting"
          exit
          end

        File.delete(filename)
      end

      delete_success = delete_recording_from_zoom(file['meeting_id'], file['id'])

      unless delete_success
        puts "Failed to delete video for file #{filename}"
        exit
      end
    end
  end
end

total_pages = zoom_page_total

puts "#{total_pages} pages of videos from Zoom"

(1..total_pages).each do |page|
  retrieve_videos(page)
end
